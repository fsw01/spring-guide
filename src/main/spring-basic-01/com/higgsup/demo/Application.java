package com.higgsup.demo;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {
    
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        
        System.out.println("Let's inspect the beans provided by Spring Boot:");

        System.out.println("Total bean definitions:" + ctx.getBeanDefinitionCount());

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }

        Bean1 objBean1A = ctx.getBean(Bean1.class);
        Bean1 objBean1B = ctx.getBean(Bean1.class);

        Bean2 objBean2A = ctx.getBean(Bean2.class);
        Bean2 objBean2B = ctx.getBean(Bean2.class);

        Bean3 objBean3A = ctx.getBean(Bean3.class);
        Bean3 objBean3B = ctx.getBean(Bean3.class);
    }

}
